FROM postgres:11-alpine

# cgal dependency
ENV GMP_VERSION=6.1.2
ENV GMP_SHA256=5275bb04f4863a13516b2f39392ac5e272f5e1bb8057b18aec1c9b79d73d8fb2

# cgal dependency
ENV MPFR_VERSION=4.0.1
ENV MPFR_SHA256=e650f8723bfc6eca4f222c021db3d5d4cebe2e21c82498329bb9e6815b99c88c

# pgrouting dependency
ENV CGAL_VERSION=4.13
ENV CGAL_SHA256=c4912a00e99f29ee37cac1d780d115a048743370b9329a2cca839ffb392f3516

# postgis dependency
ENV GEOS_VERSION=3.7.0
ENV GEOS_SHA256=c4572cff40f52aad43bf3cccc3f81364f9e842f9b4b66ef23a28855037663f91

# postgis dependency
ENV GDAL_VERSION=2.3.2
ENV GDAL_SHA256=e0f751bff9ba6fb541065acbe7a76007be76a3c6309240faf4e6440f6ff1702a

# postgis dependency
ENV PROJ4_VERSION=5.2.0
ENV PROJ4_SHA256=d784d51a8e56282123eb5918a2c685ed7da5097595afcacf5fa0246337a44361

# postgis dependency
ENV PROTOBUFC_VERSION=1.3.1
ENV PROTOBUFC_SHA256=5eeec797d7ff1d4b1e507925a1780fad5dd8dd11163203d8832e5a9f20a79b08

ENV POSTGIS_VERSION 2.5.0
ENV POSTGIS_SHA256 35169b7eb733262ae557097e3a68dc9d5b35484e875c37b4fd3372fcc80c39b9

ENV PGROUTING_VERSION 2.6.1
ENV PGROUTING_SHA256 c45211a7f9db9fa58a1593b65a23eb892e39ade3e6777e0bdd1e63c1b49241d3

ENV OSM2PGSQL_VERSION 0.96.0
ENV OSM2PGSQL_SHA256 b6020e77d88772989279a69ae4678e9782989b630613754e483b5192cd39c723

RUN set -eux \
  \
  && apk update \
  && apk upgrade --no-cache \
  && apk add --no-cache --virtual .fetch-deps \
    ca-certificates \
    openssl \
    tar \
  \
  && wget -q -O gmp.tar.bz2 "https://gmplib.org/download/gmp/gmp-${GMP_VERSION}.tar.bz2" \
  && echo "$GMP_SHA256 *gmp.tar.bz2" | sha256sum -c - \
  && mkdir -p /usr/src/gmp \
  && tar \
    --extract \
    --file gmp.tar.bz2 \
    --directory /usr/src/gmp \
    --strip-components 1 \
  && rm gmp.tar.bz2 \
  \
  && wget -q -O mpfr.tar.gz "https://www.mpfr.org/mpfr-current/mpfr-${MPFR_VERSION}.tar.gz" \
  && echo "$MPFR_SHA256 *mpfr.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/mpfr \
  && tar \
    --extract \
    --file mpfr.tar.gz \
    --directory /usr/src/mpfr \
    --strip-components 1 \
  && rm mpfr.tar.gz \
  \
  && wget -q -O cgal.tar.gz "https://github.com/CGAL/cgal/archive/releases/CGAL-$CGAL_VERSION.tar.gz" \
  && echo "$CGAL_SHA256 *cgal.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/cgal \
  && tar \
    --extract \
    --file cgal.tar.gz \
    --directory /usr/src/cgal \
    --strip-components 1 \
  && rm cgal.tar.gz \
  \
  && wget -q -O geos.tar.gz "https://github.com/libgeos/geos/archive/$GEOS_VERSION.tar.gz" \
  && echo "$GEOS_SHA256 *geos.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/geos \
  && tar \
    --extract \
    --file geos.tar.gz \
    --directory /usr/src/geos \
    --strip-components 1 \
  && rm geos.tar.gz \
  \
  && wget -q -O gdal.tar.gz "https://github.com/OSGeo/gdal/archive/v$GDAL_VERSION.tar.gz" \
  && echo "$GDAL_SHA256 *gdal.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/gdal \
  && tar \
    --extract \
    --file gdal.tar.gz \
    --directory /usr/src/gdal \
    --strip-components 1 \
  && rm gdal.tar.gz \
  \
  && wget -q -O proj4.tar.gz "https://github.com/OSGeo/proj.4/archive/$PROJ4_VERSION.tar.gz" \
  && echo "$PROJ4_SHA256 *proj4.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/proj4 \
  && tar \
    --extract \
    --file proj4.tar.gz \
    --directory /usr/src/proj4 \
    --strip-components 1 \
  && rm proj4.tar.gz \
  \
  && wget -q -O protobufc.tar.gz "https://github.com/protobuf-c/protobuf-c/archive/v$PROTOBUFC_VERSION.tar.gz" \
  && echo "$PROTOBUFC_SHA256 *protobufc.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/protobufc \
  && tar \
    --extract \
    --file protobufc.tar.gz \
    --directory /usr/src/protobufc \
    --strip-components 1 \
  && rm protobufc.tar.gz \
  \
  && wget -q -O postgis.tar.gz "https://github.com/postgis/postgis/archive/$POSTGIS_VERSION.tar.gz" \
  && echo "$POSTGIS_SHA256 *postgis.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/postgis \
  && tar \
      --extract \
      --file postgis.tar.gz \
      --directory /usr/src/postgis \
      --strip-components 1 \
  && rm postgis.tar.gz \
  \
  && wget -q -O pgrouting.tar.gz "https://github.com/pgRouting/pgrouting/archive/v$PGROUTING_VERSION.tar.gz" \
  && echo "$PGROUTING_SHA256 *pgrouting.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/pgrouting \
  && tar \
      --extract \
      --file pgrouting.tar.gz \
      --directory /usr/src/pgrouting \
      --strip-components 1 \
  && rm pgrouting.tar.gz \
  \
  && wget -q -O osm2pgsql.tar.gz "https://github.com/openstreetmap/osm2pgsql/archive/$OSM2PGSQL_VERSION.tar.gz" \
  && echo "$OSM2PGSQL_SHA256 *osm2pgsql.tar.gz" | sha256sum -c - \
  && mkdir -p /usr/src/osm2pgsql \
  && tar \
      --extract \
      --file osm2pgsql.tar.gz \
      --directory /usr/src/osm2pgsql \
      --strip-components 1 \
  && rm osm2pgsql.tar.gz \
  \
  && apk add --no-cache --virtual .build-deps \
    build-base \
    cmake \
    linux-headers \
    expat-dev \
    zlib-dev \
    bzip2-dev \
    lua5.2-dev \
    boost-dev \
    boost-filesystem \
    boost-system \
    boost-thread \
    protobuf-dev \
    autoconf \
    automake \
    libtool \
    libxml2-dev \
    json-c-dev \
    pcre-dev \
  \
  && cd /usr/src/gmp \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/mpfr \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/cgal \
  && mkdir build \
  && cd build \
  && cmake .. -DCGAL_INSTALL_LIB_DIR=/usr/local/lib \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/geos \
  && ./autogen.sh \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/gdal/gdal \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/proj4 \
  && ./autogen.sh \
  && ./configure \
    -- prefix=/usr/local \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/protobufc \
  && ./autogen.sh \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/postgis \
  && ./autogen.sh \
  && ./configure \
  && make -j "$(nproc)" \
  && make install \
  \
  && cd /usr/src/pgrouting \
  && mkdir build \
  && cd build \
  && cmake .. \
  && make -j "$(nproc)" \
  && make install \
  \
  # fix lua detection
  && ln -s /usr/lib/lua5.2/liblua.so /usr/lib/liblua.so \
  && cd /usr/src/osm2pgsql \
  && mkdir build \
  && cd build \
  && cmake .. \
  && make -j "$(nproc)" \
  && make install \
  \
  && rm -rf \
    /usr/local/lib/cmake \
    /usr/local/lib/pkgconfig \
    /usr/src/* \
  && apk del .fetch-deps .build-deps \
  && RUN_DEPS="$( \
      scanelf --needed --nobanner --format '%n#p' /usr/local/bin/* /usr/local/lib/postgresql/* \
          | tr ',' '\n' \
          | sort -u \
          | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
  )" \
  && apk add --no-cache --virtual .run-deps \
    $RUN_DEPS

COPY initdb-postgis.sh /docker-entrypoint-initdb.d/postgis.sh
COPY update-postgis.sh /usr/local/bin
