variables:
  FORCE_BUILD:
    value: ""
    description: Force to build all jobs (true/false)

stages:
  - geos
  - proj
  - cgal
  - sfcgal
  - libpqxx
  - gdal
  - postgis
  - pgrouting
  - osm2pgsql
  - osm2pgrouting
  - combine

.build:
  tags:
    - build
  needs: []
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: ['']
  variables:
    BUILD_PATH: ${CI_PROJECT_DIR}
  before_script:
    # https://github.com/GoogleContainerTools/kaniko/issues/1297#issuecomment-1149054291
    - if [ -d "/var" ]; then mv /var /var-orig; fi
    - >
      if [ -f /etc/gitlab-runner/certs/ca.crt ]; then
        export REGISTRY_CERTIFICATE="--registry-certificate=${HARBOR_SERVER}=/etc/gitlab-runner/certs/ca.crt"
      fi
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${HARBOR_SERVER}\":{\"username\":\"${HARBOR_USER}\",\"password\":\"${HARBOR_PASSWORD}\"}}}" > /kaniko/.docker/config.json
  script:
    - /kaniko/executor $REGISTRY_CERTIFICATE --context ${BUILD_PATH} --dockerfile ${DOCKERFILE} ${BUILD_ARG} ${DESTINATION}

geos:
  stage: geos
  extends:
    - .build
  needs: []
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.${CI_JOB_NAME}
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

proj:
  stage: proj
  tags:
    - bigmem
    - build
  extends:
    - .build
  needs: []
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.${CI_JOB_NAME}
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

cgal:
  stage: cgal
  extends:
    - .build
  needs: []
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.${CI_JOB_NAME}
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

sfcgal:
  stage: sfcgal
  tags:
    - bigmem
    - build
  extends:
    - .build
  needs:
    - cgal
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.${CI_JOB_NAME}
    BUILD_ARG: --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

{{ range $i, $version := .Postgres }}
libpqxx:postgres-{{ $version }}:
  stage: libpqxx
  extends:
    - .build
  needs: []
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.libpqxx
    BUILD_ARG: --build-arg POSTGRES_VERSION={{ $version }} --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

gdal:postgres-{{ $version }}:
  stage: gdal
  extends:
    - .build
  needs:
    - proj
    - geos
    - cgal
    - sfcgal
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.gdal
    BUILD_ARG: --build-arg POSTGRES_VERSION={{ $version }} --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

postgis:postgres-{{ $version }}:
  stage: postgis
  extends:
    - .build
  needs:
    - proj
    - geos
    - cgal
    - sfcgal
    - gdal:postgres-{{ $version }}
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.postgis
    BUILD_ARG: --build-arg POSTGRES_VERSION={{ $version }} --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

pgrouting:postgres-{{ $version }}:
  stage: pgrouting
  extends:
    - .build
  needs:
    - libpqxx:postgres-{{ $version }}
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.pgrouting
    BUILD_ARG: --build-arg POSTGRES_VERSION={{ $version }} --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

osm2pgrouting:postgres-{{ $version }}:
  stage: osm2pgrouting
  extends:
    - .build
  needs:
    - libpqxx:postgres-{{ $version }}
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.osm2pgrouting
    BUILD_ARG: --build-arg POSTGRES_VERSION={{ $version }} --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

osm2pgsql:postgres-{{ $version }}:
  stage: osm2pgsql
  extends:
    - .build
  needs:
    - proj
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.osm2pgsql
    BUILD_ARG: --build-arg POSTGRES_VERSION={{ $version }} --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/build_cache/postgis-${CI_JOB_NAME}

combine:postgres-{{ $version }}:
  stage: combine
  extends:
    - .build
  needs:
    - geos
    - proj
    - cgal
    - sfcgal
    - libpqxx:postgres-{{ $version }}
    - gdal:postgres-{{ $version }}
    - postgis:postgres-{{ $version }}
    - pgrouting:postgres-{{ $version }}
    - osm2pgrouting:postgres-{{ $version }}
    - osm2pgsql:postgres-{{ $version }}
  variables:
    DOCKERFILE: ${CI_PROJECT_DIR}/dist/Dockerfile.osm2pgsql
    BUILD_ARG: --build-arg POSTGRES_VERSION={{ $version }} --build-arg PROXY_CACHE_PREFIX=${HARBOR_SERVER}/proxy_cache/ --build-arg BUILD_CACHE_PREFIX=${HARBOR_SERVER}/build_cache/
    DESTINATION: --destination ${HARBOR_SERVER}/ideaplexus/postgis:postgres-{{ $version }}{{if eq $version (index $.Postgres 0) }} --destination ${HARBOR_SERVER}/ideaplexus/postgis:latest{{ end }}
{{ end }}